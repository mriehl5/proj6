"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

BREVET_TIMES = {200: 13.5, 300: 20, 400: 27, 600: 40, 1000: 75} # Valid distances: special case times
DIST = [(0, 200, 15, 34), (200, 400, 15, 32), (400, 600, 15, 30), (600, 1000, 11.428, 28)] # Distance min/max speed brackets


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    if brevet_dist_km not in BREVET_TIMES.keys():
        raise ValueError("Invalid Brevet Distance")

    control_dist_km = int(control_dist_km)
    brevet_start_time = arrow.get(brevet_start_time)

    open_t = 0
    for min_d, max_d, min_s, max_s in DIST:
        if control_dist_km > min_d:
            used = min(control_dist_km - min_d, max_d - min_d)
            open_t += used/max_s
    
    hours = int(open_t)
    minutes = round((open_t - hours) * 60)

    return brevet_start_time.shift(hours = hours, minutes = minutes).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """

    if brevet_dist_km not in BREVET_TIMES.keys():
        raise ValueError("Invalid Brevet Distance")

    control_dist_km = int(control_dist_km)
    brevet_start_time = arrow.get(brevet_start_time)

    if control_dist_km == 0:
        return brevet_start_time.shift(hours =+ 1).isoformat()

    if control_dist_km == brevet_dist_km:
        return brevet_start_time.shift(hours = BREVET_TIMES[brevet_dist_km]).isoformat()

    close_t = 0
    for min_d, max_d, min_s, max_s in DIST:
        if control_dist_km > min_d:
            used = min(control_dist_km - min_d, max_d - min_d)
            close_t += used/min_s
    hours = int(close_t)
    minutes = round((close_t - hours) * 60)

    return brevet_start_time.shift(hours = hours, minutes = minutes).isoformat()

