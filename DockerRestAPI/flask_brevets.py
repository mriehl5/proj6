"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
from pymongo import MongoClient
import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging
import os
import itertools


###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevetsdb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.route("/brevets")
@app.route("/brevets/<int:brevet_id>")
def brevets(brevet_id=None):
    brevets = [brevet for brevet in db.brevets.find()]
    print(brevets)
    if brevet_id == None:
        return flask.render_template('brevet_list.html', brevets=brevets)

    elif brevet_id >= len(brevets):
        return flask.render_template('404.html'), 404
    
    controls = db.controls.find({"id":brevet_id})
    return flask.render_template('brevet.html', brevet=brevets[brevet_id], controls=controls)
    


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('dist', 1000, type=int)
    time = request.args.get('time', '')
    date = request.args.get('date', '')
    starting = arrow.get(f"{date}T{time}").isoformat()
    print(f"{time}\n{date}\n{starting}")
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, dist, starting)
    close_time = acp_times.close_time(km, dist, starting)
    print(f"{open_time}\n{close_time}")
    result = {"open": open_time, "close": close_time}
    
    return flask.jsonify(result=result)

@app.route("/_submit", methods=["POST"])
def _submit():
    """stores entries in a database"""

    app.logger.debug("Got a JSON request")
    req = request.get_json()

    if req["rows"][0][0] != "0" and req["rows"][0][0] != "0.0":
        return flask.jsonify(error=True, message="First control should be at distance 0.0 i.e. the start")

    new_id = db.brevets.count_documents({})
    rows = req.pop("rows")
    end = False
    filtered_rows = [{"id":new_id, "km":0, "mi":0, "loc":rows[0][2], "open":rows[0][3], "close":rows[0][4]}]
    last_dist = 0
    for row in rows[1:]:
        dist = row[0]
        if not end:
            if dist == "":
                end = True
            else:
                try:
                    dist_val = int(float(dist))
                    miles = int(float(row[1]))
                except:
                    return flask.jsonify(error=True, message="Distace not a valid number")

                if dist_val <= last_dist:
                    return flask.jsonify(error=True, message="Control distances must be further than the last")
                else:
                    last_dist = dist_val
                    filtered_rows.append({"id":new_id, "km":dist_val, "mi":miles, "loc": row[2], "open":row[3], "close":row[4]})
        else:
            if dist != "":
                return flask.jsonify(error=True, message="You have a gap in your control points")
        
    if last_dist != req["dist"]:
        return flask.jsonify(error=True, message="Final contol must equal overall brevet distance")

    for row in filtered_rows:
        starting = arrow.get(f"{req['date']}T{req['time']}").isoformat()
        open_time = acp_times.open_time(row['km'], int(req['dist']), starting)
        close_time = acp_times.close_time(row['km'], int(req['dist']), starting)
        row["open"] = open_time
        row["close"] = close_time
        db.controls.insert_one(row)

    db.brevets.insert_one(req)

    return flask.jsonify(error=False)

def grouped_controls(controls, brevets, open=True, close=True):
    out_dict = {"brevets":[]}

    for k, v in itertools.groupby(controls, lambda x : x["id"]):
        brevet = brevets[k]
        ctrls = list(v)
        for ctrl in ctrls:
            ctrl.pop("id")
            ctrl.pop("_id")
            if not open:
                ctrl.pop("open")
            if not close:
                ctrl.pop("close")
        append_dict = {"distance":brevet["dist"], "begin_date":brevet["date"], "begin_time":brevet["time"], "controls":ctrls}
        out_dict["brevets"].append(append_dict)

    return out_dict

@app.route("/listAll")
@app.route("/listAll/json")
def list_all():
    controls = [control for control in db.controls.find()]
    brevets = [brevet for brevet in db.brevets.find()]

    out_dict = grouped_controls(controls, brevets)
    print(out_dict)

    return flask.jsonify(**out_dict)

@app.route("/listAll/csv")
def list_all_csv():
    controls = [control for control in db.controls.find()]
    brevets = [brevet for brevet in db.brevets.find()]
    
    out_str = 'brevets/distance,brevets/begin_date,brevets/begin_time,brevets/controls/0/km,brevets/controls/0/mi,brevets/controls/0/location,brevets/controls/0/open,brevets/controls/0/close'
    
    for control in controls:
        id = control.pop("id")
        brevet = brevets[id]
        out_str += f'\n{brevet["dist"]},{brevet["date"]},{brevet["time"]},{control["km"]},{control["mi"]},{control["loc"]},{control["open"]},{control["close"]}'

    print(out_str)

    return out_str
    

@app.route("/listOpenOnly")
@app.route("/listOpenOnly/json")
def open_only():
    controls = [control for control in db.controls.find()]
    brevets = [brevet for brevet in db.brevets.find()]
    
    if 'top' not in request.args:
        out_dict = grouped_controls(controls, brevets, close=False)
    else:
        controls.sort(key = lambda x: arrow.get(x['open']))
        num = min(len(controls), int(request.args['top']))

        out_dict = {'brevets':[]}
        for i in range(num):
            ctrl = controls[i]
            brevet = brevets[ctrl.pop("id")]
            ctrl.pop("close")
            ctrl.pop("_id")
            append_dict = {"distance":brevet["dist"], "begin_date":brevet["date"], "begin_time":brevet["time"], "control":ctrl}
            out_dict['brevets'].append(append_dict)

    return flask.jsonify(**out_dict)

@app.route("/listOpenOnly/csv")
def list_open_csv():
    controls = [control for control in db.controls.find()]
    brevets = [brevet for brevet in db.brevets.find()]

    if 'top' in request.args:
        controls.sort(key = lambda x: arrow.get(x['open']))
        num = min(len(controls), int(request.args['top']))
        controls = controls[:num]
    
    out_str = 'brevets/distance,brevets/begin_date,brevets/begin_time,brevets/controls/0/km,brevets/controls/0/mi,brevets/controls/0/location,brevets/controls/0/open'
    
    for control in controls:
        id = control.pop("id")
        brevet = brevets[id]
        out_str += f'\n{brevet["dist"]},{brevet["date"]},{brevet["time"]},{control["km"]},{control["mi"]},{control["loc"]},{control["open"]}'
    
    return out_str
    

@app.route("/listCloseOnly")
@app.route("/listCloseOnly/json")
def close_only():
    controls = [control for control in db.controls.find()]
    brevets = [brevet for brevet in db.brevets.find()]
    
    if 'top' not in request.args:
        out_dict = grouped_controls(controls, brevets, open=False)
    else:
        controls.sort(key = lambda x: arrow.get(x['close']))
        num = min(len(controls), int(request.args['top']))

        out_dict = {'brevets':[]}
        for i in range(num):
            ctrl = controls[i]
            brevet = brevets[ctrl.pop("id")]
            ctrl.pop("open")
            ctrl.pop("_id")
            append_dict = {"distance":brevet["dist"], "begin_date":brevet["date"], "begin_time":brevet["time"], "control":ctrl}
            out_dict['brevets'].append(append_dict)

    return flask.jsonify(**out_dict)

@app.route("/listCloseOnly/csv")
def list_close_csv():
    controls = [control for control in db.controls.find()]
    brevets = [brevet for brevet in db.brevets.find()]

    if 'top' in request.args:
        controls.sort(key = lambda x: arrow.get(x['close']))
        num = min(len(controls), int(request.args['top']))
        controls = controls[:num]
    
    out_str = 'brevets/distance,brevets/begin_date,brevets/begin_time,brevets/controls/0/km,brevets/controls/0/mi,brevets/controls/0/location,brevets/controls/0/close'
    
    for control in controls:
        id = control.pop("id")
        brevet = brevets[id]
        out_str += f'\n{brevet["dist"]},{brevet["date"]},{brevet["time"]},{control["km"]},{control["mi"]},{control["loc"]},{control["close"]}'
    
    return out_str

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
