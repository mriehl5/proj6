"""
Nose tests for acp_times
"""

import acp_times
import arrow

import nose
import logging
logging.basicConfig(format='%(levelname)s:%(messsage)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

now = arrow.get("2000-01-01T00:00:00")

def test_open_time():
    assert acp_times.open_time(400, 600, now.isoformat()) == "2000-01-01T12:08:00+00:00"

def test_close_time():
    assert acp_times.close_time(400, 600, now.isoformat()) == "2000-01-02T02:40:00+00:00"

def test_km_truncate(): 
    assert acp_times.open_time(400, 600, now.isoformat()) == acp_times.open_time(400.9, 600, now.isoformat())

def test_brevet_time(): 
    assert acp_times.close_time(400, 400, now.isoformat()) == "2000-01-02T03:00:00+00:00"

def test_final_bracket():
    assert acp_times.open_time(950, 1000, now.isoformat()) == "2000-01-02T07:18:00+00:00"
    assert acp_times.close_time(950, 1000, now.isoformat()) == "2000-01-03T22:38:00+00:00"

def test_start_control():
    assert acp_times.close_time(0, 600, now.isoformat()) == "2000-01-01T01:00:00+00:00"