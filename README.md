# Project 6: Brevet time calculator Adding Submit and Display buttons

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). The description is ambiguous, but the examples help.  

We are essentially replacing the calculator here (https://rusa.org/octime_acp.html). We can also use that calculator to clarify requirements and develop test data.  

### Table for minimum and maximum speeds for ACP brevets

| Control Location(km) | Minimum Speed(km/hr) | Maximum Speed(km/hr) |
|:---:|:---:|:---:|
|0-200|15|34|
|200-400|15|32|
|400-600|15|30|
|600-1000|11.482|28|

## Clarifying Information

* Times and distances are rounded to the nearest whole number
* The min/max speeds are used in brackets:
	* A 300km brevet will use the same minimum speed for the whole distance but a max speed of 34 km/hr for the first 200 km and a max speed of 32 km/hrfor the last 100 km
	* The previous bracket will be used in it's entirety before moving to the next bracket
* By rule the closing time for the starting control is one hour after after the official start time
	* It is possible to have the first control after the start close before the start closes (admin should avoid this)
* The Closing times for the end of the brevet are not calculated as above and instead are as listed below (hr:min):
	* 13:30 for 200km
	* 20:00 for 300km
	* 27:00 for 400km
	* 40:00 for 600km
	* 75:00 for 1000km

## Submit and Display buttons

### Submit
After entering all of the relevant data you can click on the submit button to save the information. Including start date/time, overall distance, and the relevant control distances and times.

You can use the submit button multiple times to save multiple brevets.

### Display
When you're ready to look at your saved brevets click on Display. You will be taken to a new page with links to the brevets you have save (Number - Brevet Date). If you wish to add more brevets you can go back to the calculator to do so.

## Test Cases
* Submit
	* The first control point should be at distance 0
	* The control distances need to increase from the last
	* The last control distance needs to be the same as the overall brevet distance
	
* Display
	* If you were to manually enter a URL to see saved race data and that URL is at an index higher than the total saved brevets then a 404 error should display.
	
	
## Functionality added

This project has following four parts. Change the values for host and port according to your machine, and use the web browser to check the results.

* You will design RESTful service to expose what is stored in MongoDB. Specifically, you'll use the boilerplate given in DockerRestAPI folder, and create the following three basic APIs:
    * `http://<host:port>/listAll` should return all open and close times in the database
    * `http://<host:port>/listOpenOnly` should return open times only
    * `http://<host:port>/listCloseOnly` should return close times only

* You will also design two different representations: one in csv and one in json. For the above three basic APIs, JSON should be your default representation. 
    * `http://<host:port>/listAll/csv` should return all open and close times in CSV format
    * `http://<host:port>/listOpenOnly/csv` should return open times only in CSV format
    * `http://<host:port>/listCloseOnly/csv` should return close times only in CSV format

    * `http://<host:port>/listAll/json` should return all open and close times in JSON format
    * `http://<host:port>/listOpenOnly/json` should return open times only in JSON format
    * `http://<host:port>/listCloseOnly/json` should return close times only in JSON format

* You will also add a query parameter to get top "k" open and close times. For examples, see below.

    * `http://<host:port>/listOpenOnly/csv?top=3` should return top 3 open times only (in ascending order) in CSV format 
    * `http://<host:port>/listOpenOnly/json?top=5` should return top 5 open times only (in ascending order) in JSON format
    * `http://<host:port>/listCloseOnly/csv?top=6` should return top 5 close times only (in ascending order) in CSV format
    * `http://<host:port>/listCloseOnly/json?top=4` should return top 4 close times only (in ascending order) in JSON format

* You'll also design consumer programs (e.g., in jQuery) to use the service that you expose. "website" inside DockerRestAPI is an example of that. It is uses PHP. You're welcome to use either PHP or jQuery to consume your services. NOTE: your consumer program should be in a different container like example in DockerRestAPI.

## Data Samples

The sample data files ([sample-data.json](data-samples/sample-data.json), [sample-data.csv](data-samples/sample-data.csv), and [sample-data-pivoted.csv](data-samples/sample-data-pivoted.csv)) provide a suggested JSON and CSV format that you could follow for your exports. 

1. JSON
```json
{
   "brevets":[
      {
         "distance":200,
         "begin_date":"12/01/2021",
         "begin_time":"18:06",
         "controls":[
            {
               "km":0,
               "mi":0,
               "location":"begin",
               "open":"12/01/2021 18:06",
               "close":"12/01/2021 19:06"
            },
            {
               "km":100,
               "mi":62,
               "location":null,
               "open":"12/01/2021 21:02",
               "close":"12/02/2021 00:46"
            },
            {
               "km":150,
               "mi":93,
               "location":"second checkpoint",
               "open":"12/01/2021 22:31",
               "close":"12/02/2021 04:06"
            },
            {
               "km":200,
               "mi":124,
               "location":"last checkpoint",
               "open":"12/01/2021 23:59",
               "close":"12/02/2021 07:36"
            }
         ]
      },
      {
         "distance":1000,
         "begin_date":"01/01/2022",
         "begin_time":"00:00",
         "controls":[
            {
               "km":0,
               "mi":0,
               "location":"begin",
               "open":"01/01/2022 00:00",
               "close":"01/01/2022 01:00"
            },
            {
               "km":1000,
               "mi":621,
               "location":"finish line",
               "open":"01/01/2022 09:05",
               "close":"01/04/2022 03:00"
            }
         ]
      }
   ]
}
```

2. CSV
```csv
brevets/distance,brevets/begin_date,brevets/begin_time,brevets/controls/0/km,brevets/controls/0/mi,brevets/controls/0/location,brevets/controls/0/open,brevets/controls/0/close,brevets/controls/1/km,brevets/controls/1/mi,brevets/controls/1/location,brevets/controls/1/open,brevets/controls/1/close,brevets/controls/2/km,brevets/controls/2/mi,brevets/controls/2/location,brevets/controls/2/open,brevets/controls/2/close,brevets/controls/3/km,brevets/controls/3/mi,brevets/controls/3/location,brevets/controls/3/open,brevets/controls/3/close
200,12/01/2021,18:06,0,0,begin,12/01/2021 18:06,12/01/2021 19:06,100,62,,12/01/2021 21:02,12/02/2021 00:46,150,93,second checkpoint,12/01/2021 22:31,12/02/2021 04:06,200,124,last checkpoint,12/01/2021 23:59,12/02/2021 07:36
1000,01/01/2022,00:00,0,0,begin,01/01/2022 00:00,01/01/2022 01:00,1000,621,finish line,01/01/2022 09:05,01/04/2022 03:00,,,,,,,,,,
```



Author: Meghan Riehl
Contact: mriehl5@uoregon.edu
